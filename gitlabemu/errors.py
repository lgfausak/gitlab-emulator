# vim:ft=ansible:tabstop=8 expandtab shiftwidth=4 softtabstop=4
"""
Base error types
"""


class GitlabEmulatorError(Exception):
    """
    Common base for all errors we raise
    """
    pass
