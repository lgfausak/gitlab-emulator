# vim:ft=ansible:tabstop=8 expandtab shiftwidth=4 softtabstop=4
"""
Main module entrypoint for gitlabemu
"""

from .runner import run
run()
